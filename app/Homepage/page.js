import React from 'react'
import Image from 'next/image';
import HeroImage from '../../Assets/heroimage.png'
import Tick from '../../Assets/tick.png'
import Clients from '../../Assets/clients.png'
import Design1 from '../../Assets/design1.png'
import Design2 from '../../Assets/design2.png'
import Design3 from '../../Assets/design3.png'
import man1 from '../../Assets/man1.png'
import girl1 from '../../Assets/girl1.png'
import girl2 from '../../Assets/girl2.png'
import sec1 from '../../Assets/sec1.png'
import sec2 from '../../Assets/sec2.png'
import sec3 from '../../Assets/sec3.png'
import quote1 from '../../Assets/quote1.png'
import quote2 from '../../Assets/quote2.png'
import test from '../../Assets/test.png'

import no1 from '../../Assets/no1.png'
import no2 from '../../Assets/no2.png'
import no3 from '../../Assets/no3.png'
import Link from 'next/link';

import './page.css'
const page = () => {
    return (
        <>
            {/* section 1 of homepage */}
            <section style={{ background: "var(--grey-light)" }} className='flex flex-col'>
                <div>
                    <h1 className='text-5xl font-bold capitalize text-center p-8 px-52 pt-8 md:text-3xl md:px-10 lg:px-40 sm:text-3xl sm:px-8'>
                        A UI/UX design subscription so <span style={{ color: "var(--orange)" }}> good </span> <br className='md:hidden'></br>
                        it should come with a <span style={{ color: "var(--orange)" }}> warning </span>label
                    </h1>
                    <div className='flex justify-center sm:flex-col sm:items-center'>
                        <div className='text-sm font-bold flex p-2 sm:text-xs'>
                            <Image className='pr-1'
                                src={Tick}
                                alt="ZigSign Logo"
                            />
                            <h3>
                                No hiring headaches
                            </h3>
                        </div>
                        <div className='text-sm font-bold flex p-2 sm:text-xs'>
                            <Image className='pr-1'
                                src={Tick}
                                alt="ZigSign Logo"
                            />
                            <h3>
                                Weekly turnaround times
                            </h3>
                        </div>
                        <div className='text-sm font-bold flex p-2 sm:text-xs'>
                            <Image className='pr-1'
                                src={Tick}
                                alt="ZigSign Logo"
                            />
                            <h3>
                                One flat monthly rate
                            </h3>
                        </div>
                    </div>
                    <div className='text-center pt-4 '>
                        <Link href={"/"}> <button style={{ background: "var(--orange)" }} className='uppercase font-bold  p-3 px-8 rounded-2xl text-white sm:text-xs'>start your risk free trial</button> </Link>
                    </div>
                </div>
                <div className='w-3/5 flex justify-center self-center md:w-3/5 sm:w-4/5 '>
                    <Image
                        src={HeroImage}
                        alt="ZigSign Logo"
                    />
                </div>
            </section>



            {/* section 2 of homepage */}
            <section style={{ background: "var(--dark-brown)", marginTop: "-5px" }} className='p-20 sm:p-8 sm:py-16'>
                <div className='text-4xl font-bold capitalize text-center p-8 px-52 pt-8 md:text-3xl md:px-10 text-white md:px-0 sm:px-0 lg:px-8 sm:text-3xl sm:px-0'>
                    We helped our customers raise <span style={{ color: "var(--orange)" }}>$1B+ </span>
                </div>

                <div className='flex justify-center'>
                    <Image className='max-w-5xl md:max-w-3xl sm:w-80'
                        src={Clients}
                        alt="ZigSign Clients image"
                    />
                </div>
            </section>


            {/* section 3 of homepage */}
            <section >
                <div className='pt-16 px-4'>
                    <div className='text-4xl font-bold capitalize text-center p-4 px-52 pt-8 md:text-3xl md:px-10 md:px-0 sm:px-0 lg:px-28 sm:text-3xl sm:px-0'>
                        A diverse team of  <span style={{ color: "var(--orange)" }}>passionate </span>  UX nerds who are here to help your <span style={{ color: "var(--orange)" }}>startup succeed </span>
                    </div>
                    <div className='text-base font-normal text-center p-4 px-52 pt-4 md:text-sm md:px-4 sm:pt-0 sm:text-xs lg:px-28 sm:px-8'>
                        From ideation to expansion, TDP understands the unique demands of your startup.
                    </div>
                </div>

                <div className='flex px-28 pb-16 md:flex md:flex-col lg:px-8 md:px-0 sm:text-4xl sm:px-8 sm:flex-col'>
                    <div className='flex flex-1 flex-col justify-start items-center p-16 lg:px-4 md:px-12 md:py-8 sm:py-4'>
                        <Image className='pr-1 w-40'
                            src={Design1}
                            alt="ZigSign Logo"
                        />
                        <h3 className='text-lg font-bold flex p-2 sm:text-xs'>
                            Costs 70% less
                        </h3>
                        <p className='text-sm font-normal text-center flex p-2 sm:text-xs'>We developed highly-optimized design processes to let us work fast and deliver incredible results—and we pass those savings on to you </p>
                    </div>

                    <div className='flex flex-1 flex-col justify-start items-center p-16 lg:p-4 md:px-12 md:py-4 sm:py-4'>
                        <Image className='pr-1 w-40'
                            src={Design2}
                            alt="ZigSign Logo"
                        />
                        <h3 className='text-lg font-bold flex p-2 sm:text-xs'>
                            Adaptable and scalable
                        </h3>
                        <p className='text-sm font-normal text-center flex p-2 sm:text-xs'>High-velocity is crucial to a startup’s success, and that’s why TDP delivers new designs every week so your momentum never falters</p>
                    </div>

                    <div className='flex flex-1 flex-col justify-start items-center p-16 lg:p-4 md:px-12 md:py-4 sm:py-4'>
                        <Image className='pr-1 w-40'
                            src={Design3}
                            alt="ZigSign Logo"
                        />
                        <h3 className='text-lg font-bold flex p-2 sm:text-xs'>
                            Fast turnaround times
                        </h3>
                        <p className='text-sm font-normal text-center flex p-2 sm:text-xs'>TDP was built for flexibility—we quickly adapt and grow as you grow</p>
                    </div>
                </div>
            </section>

            {/* section 4 of homepage */}
            <section>
                <div>
                    <div className='text-4xl font-bold capitalize text-center p-4 px-52 pt-8 md:text-3xl md:px-10 md:px-0 sm:px-0 lg:px-28 sm:text-3xl sm:px-4'>
                        How <span style={{ color: "var(--orange)" }}>signing up </span>  with The Design Project  <span style={{ color: "var(--orange)" }}>works </span>!?
                    </div>
                    <div className='text-base font-normal text-center p-4 px-52 pt-4 md:text-sm md:px-4 sm:pt-0 sm:text-xs lg:px-28 sm:px-4'>
                        Take a minute to imagine how you’d feel watching your user count grow, and retention rates stay consistently high. When you prioritize effective UX, better retention and growth are natural consequences. Backed by countless hours of research and testing, TDP blends UX, UI, and Product design into a deliciously smooth process that fuels productivity and success.</div>
                </div>

                <div className='flex px-28 pb-16 md:flex md:flex-col lg:px-8 md:px-0 sm:flex-col'>
                    <div className='flex flex-1 flex-col justify-around items-center p-16 lg:p-4 md:px-12 md:py-4 sm:px-0 sm:py-4'>
                        <Image className='pr-1 w-10'
                            src={no1}
                            alt="numbering 1"
                        />
                        <h3 className='text-center text-lg font-bold flex p-2 sm:text-xs' style={{ color: "var(--orange)" }}>
                            Sign up for one of our services                        </h3>
                        <Image className='pr-1 w-30 '
                            src={man1}
                            alt="ZigSign Logo"
                        />
                        <p className='text-sm font-normal text-center flex p-2 sm:text-xs'>TDP was built for flexibility—we quickly adapt and grow as you grow</p>
                    </div>

                    <div className='flex flex-1 flex-col justify-around items-center p-16 lg:p-4 md:px-12 md:py-4 sm:px-0 sm:py-4'>
                        <Image className='pr-1 w-10'
                            src={no2}
                            alt="numbering 2"
                        />
                        <h3 className='text-center text-lg font-bold flex p-2 sm:text-xs' style={{ color: "var(--orange)" }}>
                            Meet your perfect match                        </h3>
                        <Image className='pr-1 w-30 '
                            src={girl1}
                            alt="ZigSign Logo"
                        />
                        <p className='text-sm font-normal text-center flex p-2 sm:text-xs'>TDP was built for flexibility—we quickly adapt and grow as you grow</p>
                    </div>

                    <div className='flex flex-1 flex-col justify-around items-center p-16 lg:p-4 md:px-12 md:py-4 sm:px-0 sm:py-4'>
                        <Image className='pr-1 w-10'
                            src={no3}
                            alt="numbering 3"
                        />
                        <h3 className='text-center text-lg font-bold flex p-2 sm:text-xs' style={{ color: "var(--orange)" }}>
                            The first sprint begins (hold on tight)                        </h3>
                        <Image className='pr-1 w-30 '
                            src={girl2}
                            alt="ZigSign Logo"
                        />
                        <p className='text-sm font-normal text-center flex p-2 sm:text-xs'>TDP was built for flexibility—we quickly adapt and grow as you grow</p>
                    </div>
                </div>
            </section>


            {/* section 5 of homepage */}
            <section>
                <div style={{ background: "var(--dark-brown)" }} className='p-20 sm:p-4 sm:pt-12'>
                    <div className='text-4xl font-bold capitalize text-center p-4 px-52 pt-8 md:text-3xl md:px-10 md:px-0 sm:px-0 lg:px-28 text-white sm:text-3xl sm:px-0'>
                        How <span style={{ color: "var(--orange)" }}>accessible </span>  UX design is changing the <span style={{ color: "var(--orange)" }}>businesses </span>for the better
                    </div>
                </div>
                <div style={{ background: "var(--dark-brown)" }} className='flex flex-col justify-evenly items-center '>
                    <Image className='p-8 w-100'
                        src={sec2}
                        alt="numbering 3"
                    />

                    <Image className='p-8 w-100'
                        src={sec1}
                        alt="numbering 3"
                    />

                    <Image className='p-8 w-100'
                        src={sec3}
                        alt="numbering 3"
                    />
                </div>

                <div style={{ background: "var(--dark-brown)" }} className='px-80 text-center text-4xl text-white font-bold py-4 md:px-20 sm:px-8 sm:text-xl'>
                    <p>
                        <Image className='w-16 sm:w-12'
                            src={quote2}
                            alt="A picture of quote png"
                        />
                        TDP helped identify the main pain points and designed a new UX that is accessible and easy to understand. The redesign pushed us forward on our mission to be able to verify 100% of IDs in real-time, and we raised over $100M in funding.
                        <Image className='w-16 sm:w-12 text-center' style={{ marginLeft: 'auto', display: 'block' }}
                            src={quote1}
                            alt="A picture of quote png"
                        />
                    </p>

                    <div className='flex justify-center flex-col'>
                        <div className='flex justify-center items-center pt-4'>
                            <div>
                                <Image className='w-16 text-center mr-4'
                                    src={test}
                                    alt="A picture of quote png"
                                />
                            </div>
                            <p className='flex flex-col text-start text-lg' style={{ color: "var(--orange)" }}>
                                Erick Desoto <span className='text-xs font-normal' style={{ color: "var(--grey-light)" }}>VP of Product at Socure</span>
                            </p>
                        </div>
                        <div className='text-center pt-4'>
                                <Link href={"/"}>
                                    <button style={{ background: "var(--orange)" }} className='text-sm uppercase font-bold p-3 px-08 rounded-2xl text-white sm:text-sm'>start your risk-free trial</button>
                                </Link>
                            </div>
                    </div>

                </div>



            </section>


        </>
    )
}

export default page
