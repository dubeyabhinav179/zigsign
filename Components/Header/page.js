'use client'
import React, { useState } from 'react';
import { RiMenu3Line, RiCloseLine } from 'react-icons/ri'; 
import Image from 'next/image';
import logo from '../../Assets/logo.png';
import Link from 'next/link';
import './page.css';

const Header = () => {
    const [toggleMenu, setToggleMenu] = useState(false);

    return (
        <header>
        <div>
            <section className='Header-items'>
                <div className='menu-div'>
                <Link href="/">
                    <Image
                        src={logo}
                        alt="ZigSign Logo"
                    />
                    </Link>
                </div>

                <div className='menu-div'>
                    <ul className='menus-items'>
                        <li> <Link className='menus' href={"/How-It-Works"}>How It Works </Link></li>
                        <li> <Link className='menus' href={"/About"}>About </Link></li>
                        <li> <Link className='menus' href={"/Work"}>Work </Link></li>
                        <li> <Link className='menus' href={"/Pricing"}>Pricing </Link></li>
                        <li> <Link className='menus' href={"/Blog"}>Blog </Link></li>
                    </ul>
                </div>

                <div className='menu-div btn-div'>
                  <Link href="/"><button className='btn-main'>Get Started</button></Link> 
                </div>
            </section>


            {/* mobile menu  */}

            <section className='mobile-header'>
                <div className='menu-div'>
                    <Link href="/">
                    <Image
                        src={logo}
                        alt="ZigSign Logo"
                    />
                    </Link>
                </div>

            <div className="navbar-mobile">
                {toggleMenu
                    ? <RiCloseLine color="#000000" size={27} onClick={() => setToggleMenu(false)} />
                    : <RiMenu3Line color="#000000" size={27} onClick={() => setToggleMenu(true)} />}
                {toggleMenu && (
                    <div className="navbar-mobile_container scale-up-center">
                        <div className="navbar-mobile_container-links">
                            <li> <Link className='menus' href="/How-It-Works">How It Works </Link></li>
                            <li> <Link className='menus' href="/About">About </Link></li>
                            <li> <Link className='menus' href="/Work">Work </Link></li>
                            <li> <Link className='menus' href="/Pricing">Pricing </Link></li>
                            <li> <Link className='menus' href="/Blog">Blog </Link></li>
                        </div>
                    </div>
                )}
            </div>
            </section>




        </div>
        </header>
    );
};

export default Header;
