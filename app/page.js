import React from 'react'
import { Header, Footer } from '@/Components'
import HomePage from '@/app/Homepage/page'

const page = () => {
  return (
    <>
      <Header />
      <HomePage />
      <Footer />

    </>
  )
}

export default page
