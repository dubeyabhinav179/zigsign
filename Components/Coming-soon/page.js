import React from 'react'
import Link from 'next/link'
import './page.css'

const page = () => {
  return (
    <>
    <main style={{ background: 'var(--grey-light)' }} className="grid min-h-full place-items-center bg-white px-6 py-24 sm:py-32 lg:px-8">
        <div className="text-center">
            <p className="text-base font-semibold text-600" style={{ color: 'var(--orange)' }}>Coming Soon!!!!</p>
            <h1 className="mt-4 text-3xl font-bold tracking-tight text-gray-900 sm:text-5xl">Something special is on its way to you.</h1>
            <p className="mt-6 text-base leading-7 text-gray-600">Stay tuned for a symphony of innovation. Our debut is imminent!</p>
            <div className="mt-10 flex items-center justify-center gap-x-6">
               <Link href="/" style={{ background: 'var(--orange)' }}
                    className="rounded-md bg-#FA7554 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2"
                >
                    
                    Go back home
                </Link>
                <Link href="/" className="text-sm font-semibold text-gray-900">
                    Contact support <span aria-hidden="true">&rarr;</span>
                </Link>
            </div>
        </div>
    </main>

</>
  )
}

export default page
