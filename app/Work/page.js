import React from 'react'
import {Header, ComingSoon, Footer} from '@/Components'
export const metadata = {
  title: 'Work - ZigSign',
  description: 'Discover the extraordinary with our UI/UX Design Subscription – a game-changer that redefines digital experiences. Subscribe today for innovative, user-centric designs that set new standards in UI/UX. Elevate your online presence and engage your audience like never before.',
  openGraph: {
    images: [
      {
        url: 'https://opengraph.b-cdn.net/production/documents/7bea82ef-cb05-4516-bff1-44d6a38b0e62.png?token=alkgUi8uK8hWMfXIYEqPdqKPQuc_XHG-ST_KIQNNnRg&height=622&width=1200&expires=33239790900',
        alt: 'ZigSign opengraph Image',
      },
    ],
  },
};


const page = () => {
  return (
    <>
    <Header/>
    <ComingSoon/>
    <Footer/>
    </>
    
  )
}

export default page
