import React from 'react'
import Email from '../../Assets/email.png'
import Insta from '../../Assets/insta.png'
import Linkedin from '../../Assets/linkedin.png'
import Image from 'next/image'
import Link from 'next/link'

const page = () => {
    return (
        <>
            <footer>
                <section className='py-20 flex justify-around sm:flex-col sm:p-20 sm:pb-8' style={{ background: "var(--dark-brown)" }}>

                    <div className='flex flex-col items-start sm:items-center'>
                        <h1 className='text-white text-5xl font-bold capitalize text-left md:text-3xl sm:text-center'>
                            The ZigSign <span  className='text-sm font-medium'><br></br>( Under Development ) </span>
                        </h1>
                        <div className='flex justify-left py-8 md:flex-col sm:flex-col md:items-center'>
                            <div className='text-sm flex p-2 sm:text-xs'>
                                <Image className='pr-1'
                                    src={Email}
                                    alt="ZigSign email png"
                                />
                                <h3 className='text-white'>
                                    hello@ZigSign.com
                                </h3>
                            </div>
                            <div className='text-sm flex p-2 sm:text-xs'>
                                <Image className='pr-1'
                                    src={Insta}
                                    alt="ZigSign instagram png"
                                />
                                <h3 className='text-white'>
                                    @ZigSign
                                </h3>
                            </div>
                            <div className='text-sm flex p-2 sm:text-xs'>
                                <Image className='pr-1'
                                    src={Linkedin}
                                    alt="ZigSign linkedin png"
                                />
                                <h3 className='text-white'>
                                    The ZigSign
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div className='flex justify-center'>
                        <div className=''>
                            <ul className='text-right text-white sm:text-center'>
                                <li> <Link className='menus' href={"/How-It-Works"}>How It Works </Link></li>
                                <li> <Link className='menus' href={"/About"}>About </Link></li>
                                <li> <Link className='menus' href={"/Work"}>Work </Link></li>
                                <li> <Link className='menus' href={"/Pricing"}>Pricing </Link></li>
                                <li> <Link className='menus' href={"/Blog"}>Blog </Link></li>
                            </ul>
                        </div>
                    </div>

                </section>

                <section style={{ background: "var(--dark-brown)" }} className='pt-0 pb-16 px-28 flex justify-around sm:flex-col sm:p-8 sm:pb-12 text-white text-center'>

                    <p>&copy; 2023 <span style={{ color: "var(--orange)" }}> ZigSign. </span>All rights reserved. | Developed by <span style={{ color: "var(--orange)" }}> Abhinav Dubey </span> with 👾 using React.js, Next.js, and Tailwind CSS | Design inspired by <span style={{ color: "var(--orange)" }}> <a href="https://www.figma.com/file/qBbiMDgph1YYJpvd1hI5YS/SaaS-Landing-Page-%7C-Free-Website-Design-%7C-SaaS-Website-Design-(Community)?type=design&node-id=41%3A266&mode=dev" target="_blank" rel="noopener noreferrer" className='underline' >Figma</a></span></p>

                </section>
            </footer>
        </>
    )
}

export default page
